package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.models.Invoice;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.pacifica.calculetaxe.common.constant.FileConstant.*;

public final class FileUtil {

    public static File createNewFile(Invoice invoice) {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        return new File(TEST_PATH + FILE_NAME + invoice.getTotaleProductsPrices() + "_" + dateFormat.format(date) + TXT_EXTENSION);
    }

    public static void createTextInFile(File file, String text) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print(text);
        printWriter.close();
    }

    private FileUtil() {
    }
}
