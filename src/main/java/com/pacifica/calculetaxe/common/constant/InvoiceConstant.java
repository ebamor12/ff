package com.pacifica.calculetaxe.common.constant;

public final class InvoiceConstant {

    public static final String TAXE_TOTAL                                       ="Montant des taxes : ";
    public static final String TOTAL_PRIX                                       ="Total : ";
    public static final String SPACE_STRING                                     =" ";
    public static final String START_INVOICE_STRING                             ="* ";
    public static final String EURO_IN_INVOICE                                  ="€ : ";

    private  InvoiceConstant(){

    }
}
