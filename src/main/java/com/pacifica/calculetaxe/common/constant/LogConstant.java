package com.pacifica.calculetaxe.common.constant;

public final class LogConstant {

    public static final String CREATE_ORDER_LOG                                ="Create order from file: ";
    public static final String CREATE_INVOICE_FILE_LOG                         ="Create invoice file with totale TTC: ";
    public static final String FILE_EXIST_LOG                                  ="File already exist at location: " ;

    private LogConstant(){

    }
}
