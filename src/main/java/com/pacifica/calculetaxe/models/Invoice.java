package com.pacifica.calculetaxe.models;

import java.math.BigDecimal;
import java.util.List;

public class Invoice {

    private List<InvoiceDetails> invoiceDetails;
    private BigDecimal totalProductsTaxe;
    private BigDecimal totaleProductsPrices;

    public List<InvoiceDetails> getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(List<InvoiceDetails> invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    public BigDecimal getTotalProductsTaxe() {
        return totalProductsTaxe;
    }

    public void setTotalProductsTaxe(BigDecimal totalProductsTaxe) {
        this.totalProductsTaxe = totalProductsTaxe;
    }

    public BigDecimal getTotaleProductsPrices() {
        return totaleProductsPrices;
    }

    public void setTotaleProductsPrices(BigDecimal totaleProductsPrices) {
        this.totaleProductsPrices = totaleProductsPrices;
    }
}
