package com.pacifica.calculetaxe.models;

import java.util.List;

public class Order {

    private List<OrderDetails> orderDetails;

    public List<OrderDetails> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Order(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Order() {
    }
}
