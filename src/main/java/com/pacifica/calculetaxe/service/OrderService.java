package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.Order;

import java.io.IOException;

public interface OrderService {

    Order createOrderFromFile(String fileConfigPath, OrderDetailsService orderDetailsService, ProductService productService) throws IOException;
}
