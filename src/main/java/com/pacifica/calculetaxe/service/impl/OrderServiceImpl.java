package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.models.OrderDetails;
import com.pacifica.calculetaxe.service.OrderDetailsService;
import com.pacifica.calculetaxe.service.OrderService;
import com.pacifica.calculetaxe.service.ProductService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.pacifica.calculetaxe.common.constant.LogConstant.CREATE_ORDER_LOG;

public class OrderServiceImpl implements OrderService {
    private static final Logger LOGGER = Logger.getLogger(OrderServiceImpl.class.getName());

    @Override
    public Order createOrderFromFile(String fileConfigPath, OrderDetailsService orderDetailsService, ProductService productService) throws IOException {
        LOGGER.info(CREATE_ORDER_LOG + fileConfigPath);
        Order order = new Order();
        try (Stream<String> fileLines = Files.lines(Paths.get(fileConfigPath))) {
            List<OrderDetails> orderDetails = fileLines
                    .filter(Objects::nonNull)
                    .map(line -> orderDetailsService.createOrderDetailFromOrderLine(line, productService))
                    .collect(Collectors.toList());
            order.setOrderDetails(orderDetails);
        }
        return order;
    }

}
