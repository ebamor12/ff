package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.common.enumerations.ProductTypeEnum;
import com.pacifica.calculetaxe.common.utils.ProductTypeUtil;
import com.pacifica.calculetaxe.common.utils.ProductUtil;
import com.pacifica.calculetaxe.models.Product;
import com.pacifica.calculetaxe.service.ProductService;

import java.util.Map;
import java.util.Optional;

import static com.pacifica.calculetaxe.common.constant.OrderLineConstant.IMPORT_IN_LINE_ORDER;

public class ProductServiceImpl implements ProductService {

    @Override
    public Product createProductFromOrderLine(String orderLine, int quantity) {
        Product product = new Product();
        product.setImported(orderLine.contains(IMPORT_IN_LINE_ORDER));
        product.setLibelle(ProductUtil.getProductLibelleFromOrderLine(product, orderLine));
        product.setUnitPriceWithoutTaxe(ProductUtil.getProductUnitPriceFromOrderLine(orderLine));
        product.setProductType(this.getProductTypeFromProductName(product.getLibelle()));
        return product;
    }

    public ProductTypeEnum getProductTypeFromProductName(String label) {
        return Optional.of(ProductTypeUtil.getMatchingProductNames()
                .entrySet()
                .stream()
                .filter(e -> e.getValue().contains(label))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElseGet(() -> ProductTypeEnum.OTHERS))
                .get();
    }
}
