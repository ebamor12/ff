package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.OrderDetails;

import java.math.BigDecimal;
import java.util.List;

public interface TaxeCalculatorService {

    BigDecimal countProductPriceTTC(OrderDetails orderDetails);

    BigDecimal countTotalTax(List<OrderDetails> orderDetailsList);

    BigDecimal countTotalPriceTTC(List<OrderDetails> orderDetailsList);
}
